// Exercice 3 : Fonctions anonymes

const sum = (array) => {
    return array.reduce(function(acc, val) {
        return acc + val;
    }, 0);
}

const multiply = (array) => {
    return array.reduce(function(acc, val) {
        return acc * val;
    }, 1);
}

// test
console.log(sum([1,45,100])); 
console.log(multiply([1,2,3,4])); 
