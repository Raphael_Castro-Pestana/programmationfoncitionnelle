// Exercice 5 : Filtrage par compréhension

// fonction de filtrage des nombres pairs

function filterEvenNumbers(list) {
    return list.filter((num) => {
        return num % 2 === 0;
    });
}

// TESTS
const listeDeTest = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
console.log(filterEvenNumbers(listeDeTest))