// Exercice 4 : Arithmétique des fonctions

// Fonction d'addition
const add = function(f, g) {
    return function(x) {
        return f(x) + g(x);
    };
};

// Fonction de soustraction
const subtract = function(f, g) {
    return function(x) {
        return f(x) - g(x);
    };
};

// Fonction de multiplication
const multiply = function(f, g) {
    return function(x) {
        return f(x) * g(x);
    };
};

// TESTS
const f = function(x) { return x + 1; };
const g = function(x) { return x * 2; }; 

const testAdd = add(f, g);
const testSubstract = subtract(f, g);
const testMultiply = multiply(f, g); 

console.log(testAdd(2)); // 7
console.log(testSubstract(2)); //-1
console.log(testMultiply(2)); // 12