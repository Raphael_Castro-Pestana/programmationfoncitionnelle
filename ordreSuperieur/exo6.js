// Exercice 6 : fonctions paresseuses

function lazyFunction(aFunction) {
    let cacheResult; //resultat sauvegardés
    let isExecuted = false;

    return function() {
        if (!isExecuted) {
            cacheResult = aFunction();
            isExecuted = true;
        }
        return cacheResult; 
    };
}

// TESTS

const testFunction = lazyFunction(function() {
    console.log('Calculating...');
    return Math.random(); //entre 0 et 1
});

// Utilisation de la fonction paresseuse
console.log(testFunction());  // 1ere fois: "Calculating..." puis random
console.log(testFunction());  // 2ème : random recupéré de l'execution précédente sans passer par testFunction