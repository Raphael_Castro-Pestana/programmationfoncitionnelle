function curryDynamic(aFunction) {

    return function curried(...args) {

        if (args.length >= aFunction.length) {
            return aFunction(...args);
        } else {
            return function(...moreArgs) {

                return curried(...args.concat(moreArgs));
            
            };
        }

    };
}

// TESTS
function add(...args) {
    return args.reduce((acc, val) => acc + val, 0);
}

const curriedAdd = curryDynamic(add);
console.log(curriedAdd(1)); // 1
console.log(curriedAdd(1,2)); // 3
console.log(curriedAdd(1,2,3)); // 6
console.log(curriedAdd(1,2,3,4)); // 10

