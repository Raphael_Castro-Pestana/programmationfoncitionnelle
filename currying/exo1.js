// Exercice 1 : curry

function curry(aFunction) {
    return function curried(x) {
        return function(y) {
            return aFunction(x, y);
        };
    };
}

// TESTS
function add(x, y) {
    return x + y;
}

const curriedAdd = curry(add);
console.log(curriedAdd(3)(4));