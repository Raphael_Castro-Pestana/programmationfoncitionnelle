// Exercice 2  : currymultiple

function curryMultiple(aFunction) {
    
    return function curried(...args) {

        if (args.length >= aFunction.length) {
            return aFunction(...args);

        } else {
            return function(...moreArgs) {
                return curried(...args.concat(moreArgs));
            };

        }
    };

}

// TESTS
function add(a, b, c) {
    return a + b + c;
}

const curriedAdd = curryMultiple(add);
console.log(curriedAdd(2)(3)(4)); // 9
console.log(curriedAdd(2, 3)(4)); // 9
console.log(curriedAdd(2)(3, 4)); // 9
console.log(curriedAdd(2, 3, 4)); // 9