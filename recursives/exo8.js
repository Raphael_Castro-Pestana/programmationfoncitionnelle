// 8/ Pyramide de Pascal :

function generatePascalTriangle(numRows) {
    let triangle = [];

    function generateRow(row) {
        if (row === numRows) {
            return;
        }

        let currentRow = [];
        for (let i = 0; i <= row; i++) {
            if (i === 0 || i === row) {
                currentRow.push(1); 
            } else {
                let prevRow = triangle[row - 1];
                let sum = prevRow[i - 1] + prevRow[i];
                currentRow.push(sum);
            }
        }

        triangle.push(currentRow);
        generateRow(row + 1);
    }
    generateRow(0);

    return triangle;
}

// TESTS
let pascalTriangle = generatePascalTriangle(5);
console.log(pascalTriangle.map(row => row.join(' ')).join('\n'));