// 3/ Fibonacci :
function getNiemeOfFibonacci(n) {
    if (n <= 2) {
        return 1;
    } else {
        return getNiemeOfFibonacci(n - 1) + getNiemeOfFibonacci(n - 2);
    }
}

// TESTS
// 0 1 2 3 4 5 6 7  8  9  10 11 12  13  14  15 
// 0 1 1 2 3 5 8 13	21 34 55 89	144	233	377	610

console.log(getNiemeOfFibonacci(1)); // 1
console.log(getNiemeOfFibonacci(2)); // 1
console.log(getNiemeOfFibonacci(3)); // 2
console.log(getNiemeOfFibonacci(4)); // 3
console.log(getNiemeOfFibonacci(5)); // 5
console.log(getNiemeOfFibonacci(6)); // 8
console.log(getNiemeOfFibonacci(7)); // 13
console.log(getNiemeOfFibonacci(15)); // 610