// 2/ 1 2 3 soleil

function unDeuxTroisSoleil(n) {
    if (n > 20) {
        return;
    } else {
        console.log(n);
        return unDeuxTroisSoleil(n + 1);
    }
}

// TESTS

unDeuxTroisSoleil(1);
// unDeuxTroisSoleil(5);
