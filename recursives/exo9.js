// 9/ L’arbre :

// Exemple d'arbre

const treeData = {
    name: ".",
    type: "folder",
    children: [
        {
            name: "zzz.ts",
            type: "file"
        },
        {
            name: "prisma.service.ts",
            type: "file"
        },
        {
            name: "tasks",
            type: "folder",
            children: [
                {
                    name: "task.models.ts",
                    type: "file"
                },
                {
                    name: "tasks.controller.ts",
                    type: "file"
                },
                {
                    name: "task.data.ts",
                    type: "file"
                },
                {
                    name: "tasks.service.ts",
                    type: "file"
                }
            ]
        }
    ]
};

// FONCTION

function printTree(treeData, prefix = "", isLast = true) {
    console.log(`${prefix}${isLast ? "└── " : "├── "}${treeData.name}`);
    const children = treeData.children;

    if (children) {
        const count = children.length;
        children.forEach((child, index) => {

            const newPrefix = `${prefix}${isLast ? "    " : "│   "}`;
            const newIsLast = index === count - 1;
            printTree(child, newPrefix, newIsLast);
        
        });
    }
}

//TESTS
printTree(treeData);