// 6/ Tri Fusion :

function mergeSort(arr) {
    // Si vide ou un élément, OK
    if (arr.length <= 1) {
        return arr;
    }
    
    const middle = Math.floor(arr.length / 2);
    const left = arr.slice(0, middle);
    const right = arr.slice(middle);
    
    // recursive
    return merge(mergeSort(left), mergeSort(right));
}

function merge(left, right) {
    let result = [];
    let leftIndex = 0;
    let rightIndex = 0;

    while (leftIndex < left.length && rightIndex < right.length) {
        if (left[leftIndex] < right[rightIndex]) {
            result.push(left[leftIndex]);
            leftIndex++;
        } else {
            result.push(right[rightIndex]);
            rightIndex++;
        }
    }

    // Ajouter les éléments restants des deux tableaux (s'il y en a)
    return result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex));
}

// TESTS
let array = [9, 3, 4, 5, 3, 6, 2, 14, 2];
console.log("Avant tri :", array);

array = mergeSort(array);

console.log("Après tri :", array);