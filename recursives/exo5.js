// 5/ Somme des Chiffres :

function sumRecursive(num) {
    if (num < 10) {
        return num;
    } else {
        let lastNum = num % 10;
        let remainingNum = Math.floor(num / 10);        
        return lastNum + sumRecursive(remainingNum);
    }
}

// TESTS
console.log(sumRecursive(123)); // 6 (1 + 2 + 3)
console.log(sumRecursive(456)); // 15 (4 + 5 + 6)
console.log(sumRecursive(789)); // 24 (7 + 8 + 9)
