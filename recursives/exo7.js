// 7/ Générateur de Permutations :

function permutations(nums) {
    let result = [];
    
    function generatePermutations(current, remaining) {
        if (remaining.length === 0) {
            result.push(current.slice());
            return;
        }
        
        for (let i = 0; i < remaining.length; i++) {
            let next = remaining[i];
            current.push(next);
            generatePermutations(current, remaining.slice(0, i).concat(remaining.slice(i + 1)));
            current.pop();
        }
    }
    
    generatePermutations([], nums);
    
    return result;
}

// TESTS
let nums = [1, 2, 3];
let result = permutations(nums);
console.log(result);
console.log(result[0]);
console.log(result[1]);
console.log(result[2]);
console.log(result[3]);
console.log(result[4]);
console.log(result[5]);
