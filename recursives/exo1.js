// 1/ Factorielle Récursive :

function factorial(n) {
    if (n === 0) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

// TESTS
console.log(factorial(0)); // 1
console.log(factorial(5)); // 5 * 4 * 3 * 2 * 1 = 120
